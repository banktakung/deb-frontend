import { TrdebtcollectionService } from './../../../../services/trdebtcollection.service';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject, takeUntil } from 'rxjs';
import { TrfollowService } from 'src/app/services/trfollow.service';
import Swal from 'sweetalert2';
import { ModalTraceFormComponent } from './modal-trace-form/modal-trace-form.component';

@Component({
  selector: 'app-modal-trace-work',
  templateUrl: './modal-trace-work.component.html',
  styleUrls: ['./modal-trace-work.component.scss']
})
export class ModalTraceWorkComponent implements OnInit {
  public followList: any;
  private ngUnsubscribe = new Subject();
  public followListLength: number = 0;
  public userInfo = JSON.parse(localStorage.getItem("userInfo"));

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private trfollowService: TrfollowService,
    public dialog: MatDialog,
    private trdebtcollectionService: TrdebtcollectionService
  ) { }

  ngOnInit(): void {
    this.getTrace();

    this.data.documentStatus = (this.data.documentStatus === 'Y') ? true : false;
  }

  OnDestroy() {
    this.ngUnsubscribe.next(null);
    this.ngUnsubscribe.complete();
  }

  onManageTraceForm(data: any = null) {
    const dialogRef = this.dialog.open(ModalTraceFormComponent, {
      data: (data) ? data : this.data,
      width: "600px",
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getTrace();
    });

  }

  getTrace() {
    this.trfollowService.findByDebt(this.data.companyContractNumber).pipe(takeUntil(this.ngUnsubscribe)).subscribe(result => {
      if (result.serviceResult.status === "Success") {
        this.followList = result.serviceResult.value;
        this.followListLength = this.followList?.length - 1;
      } else {
        Swal.fire({
          title: "Error !",
          text: result.serviceResult.text,
          icon: "error",
          heightAuto: false
        })
      }
    }, err => {
      console.error(err);
      Swal.fire({
        title: "Error !",
        text: err.message,
        icon: "error",
        heightAuto: false
      });
    });
  }

  onUpdateDebt() {
    this.data.documentStatus = (this.data.documentStatus) ? 'Y' : 'N';
    this.data.updatedBy = this.userInfo.user.username;
    this.data.updateDate = new Date();
    this.trdebtcollectionService.createOrUpdate(this.data).subscribe(result => {
      if (result.serviceResult.status !== "Success") {
        Swal.fire({
          title: "Error !",
          text: result.serviceResult.text,
          icon: "error",
          heightAuto: false
        })
      } else {
        this.data.documentStatus = (this.data.documentStatus === 'Y') ? true : false;
      }
    }, err => {
      console.error(err);
      Swal.fire({
        title: "Error !",
        text: err.message,
        icon: "error",
        heightAuto: false
      });
    });
  }
}
