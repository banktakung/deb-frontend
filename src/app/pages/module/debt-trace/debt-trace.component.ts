import { SelectionModel } from '@angular/cdk/collections';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import * as dayjs from 'dayjs';
import { DeviceDetectorService } from 'ngx-device-detector';
import { Subject, takeUntil } from 'rxjs';
import { discountRate, minimumPayment } from 'src/app/core/middleclass';
import { NotificationEvent, syNotification } from 'src/app/model/syNotification';
import { trdebtcollection } from 'src/app/model/trdebtcollection';
import { AppConfigService } from 'src/app/services/app-config.service';
import { ModalService } from 'src/app/services/modal.service';
import { SocketService } from 'src/app/services/socket-io.service';
import { SocketInstanceService } from 'src/app/services/socket.instance.service';
import { SyuserService } from 'src/app/services/syuser.service';
import { TrdebtcollectionService } from 'src/app/services/trdebtcollection.service';
import Swal from 'sweetalert2';
import { ModalTraceSettingComponent } from '../debt-contract/modal-trace-setting/modal-trace-setting.component';
import { ModalDocumentManageComponent } from './modal-document-manage/modal-document-manage.component';
import { ModalPaymentComponentPaid } from './modal-payment/modal-payment.component';
import { ModalTelephoneListComponent } from './modal-telephone-list/modal-telephone-list.component';
import { ModalTraceWorkStatusComponent } from './modal-trace-work-status/modal-trace-work.component';
import { ModalTraceWorkComponent } from './modal-trace-work/modal-trace-work.component';

@Component({
  selector: 'app-debt-trace',
  templateUrl: './debt-trace.component.html',
  styleUrls: ['./debt-trace.component.scss']
})
export class DebtTraceComponent extends SocketService implements OnInit {
  public debtList: any;
  public debtListMock: any;
  private ngUnsubscribe = new Subject();
  public checked: boolean = false;
  public btnTrace: boolean = false;
  public userInfo = JSON.parse(localStorage.getItem("userInfo"));
  public employee: string;
  public userRole = this.userInfo?.userRole.map(x => x.roleId).join(",");


  public filter = {
    searchTxt: '',
    debtStatus: ''
  }

  public userList: any;

  public device: any;

  @Input() statusCode: any = null;

  public displayedColumns: string[] = ['select', 'companyContractNumber', 'name', 'total', 'minimumPayment', 'discountRate', 'status', 'traceBy', 'phoneList', 'action'];

  selection = new SelectionModel<any>(true, []);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public myDebtList: any;
  public myDisplayedColumns: string[] = ['companyContractNumber', 'name', 'total', 'minimumPayment', 'discountRate', 'status', 'traceBy', 'phoneList', 'action'];
  @ViewChild(MatPaginator) myPaginator: MatPaginator;
  @ViewChild(MatSort) mySort: MatSort;


  execute(notification: syNotification): void {
    switch (notification.event) {
      case NotificationEvent.reloadData:
        if (notification.formCode.includes('debtTrace') && notification.data === this.statusCode) {
          this.getDebtByStatus();
        }
        break;
      default:
        break;
    }
  }


  constructor(
    private trDebtColectionService: TrdebtcollectionService,
    public dialog: MatDialog,
    private syUserService: SyuserService,
    private deviceService: DeviceDetectorService,
    private dateAdapter: DateAdapter<Date>,
    private socketInstanceService: SocketInstanceService,
    private appConfigService: AppConfigService
  ) {
    super(socketInstanceService.getSocket(), appConfigService);
    this.dateAdapter.setLocale('th-TH');
    this.device = this.deviceService.deviceType;
  }

  ngOnInit(): void {
    this.getDebtByStatus();
    this.getUser();
    this.socketRegister("debtTrace", this.statusCode);
  }

  OnDestroy() {
    this.ngUnsubscribe.next(null);
    this.ngUnsubscribe.complete();
    this.socketDestroy();
  }

  getUser() {
    this.syUserService.findAll().pipe(takeUntil(this.ngUnsubscribe)).subscribe(result => {
      if (result.serviceResult.status === "Success") {
        this.userList = result.serviceResult.value;
      }
    })
  }

  getDebtByStatus(traceBy: string = '') {
    this.trDebtColectionService.findByStatus(this.statusCode, traceBy).pipe(takeUntil(this.ngUnsubscribe)).subscribe(result => {
      if (result.serviceResult.status === "Success") {
        this.debtList = result.serviceResult.value;
        this.myDebtList = result.serviceResult.value.filter(x => x.traceBy === this.userInfo.user.username);
        for (let debt of this.debtList) {
          debt.minimumPayment = (+debt.fee + +debt.principle) + (0.2 * (+debt.breakInterest + +debt.interestNotRecognized));
          debt.discountRate = +debt.minimumPayment / +debt.totalAmount;
        }
        for (let debt of this.myDebtList) {
          debt.minimumPayment = (+debt.fee + +debt.principle) + (0.2 * (+debt.breakInterest + +debt.interestNotRecognized));
          debt.discountRate = +debt.minimumPayment / +debt.totalAmount;
        }

        this.debtList = new MatTableDataSource(this.debtList);
        this.debtList.paginator = this.paginator;
        this.debtList.sort = this.sort;

        this.myDebtList = new MatTableDataSource(this.myDebtList);
        this.myDebtList.paginator = this.myPaginator;
        this.myDebtList.sort = this.mySort;


      } else {
        Swal.fire({
          title: "Error !",
          text: result.serviceResult.text,
          icon: "error",
          heightAuto: false
        })
      }
    }, err => {
      console.error(err);
      Swal.fire({
        title: "Error !",
        text: err.message,
        icon: "error",
        heightAuto: false
      });
    });
  }

  openManageModal(data: any = null) {
    const dialogRef = this.dialog.open(ModalTraceWorkComponent, {
      data: data,
      width: "700px",
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }

  openFreezeWork(data: any) {
    const dialogRef = this.dialog.open(ModalTraceWorkComponent, {
      data: data,
      width: "700px",
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }

  openTraceWorkStatus(data: any, status: string) {
    const dialogRef = this.dialog.open(ModalTraceWorkStatusComponent, {
      data: {
        data: data,
        status: status
      },
      width: "100%",
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }

  openSquesterWork(data: any) {
    const dialogRef = this.dialog.open(ModalTraceWorkComponent, {
      data: data,
      width: "700px",
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }

  openManageDocument(data: any = null) {
    const dialogRef = this.dialog.open(ModalDocumentManageComponent, {
      data: { statusCode: this.statusCode, data: data },
      width: "500px",
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      // this.ngOnInit();
    });
  }

  openTelephoneList(idcard: any = null) {
    const dialogRef = this.dialog.open(ModalTelephoneListComponent, {
      data: idcard,
      width: "500px"
    });

    dialogRef.afterClosed().subscribe(result => {
      // this.ngOnInit();
    });
  }

  openPaymentModal(data: any = null) {
    const dialogRef = this.dialog.open(ModalPaymentComponentPaid, {
      data: data,
      width: "700px",
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      // this.ngOnInit();
    });
  }

  checkAll() {
    let checked = !this.checked;

    if (checked) {
      for (let debt of this.debtList) {
        debt.checked = true;
      }
      this.btnTrace = true;
    } else {
      for (let debt of this.debtList) {
        debt.checked = false;
      }
      this.btnTrace = false;
    }
  }

  checkProcess() {
    if (this.debtList.filter(x => x.checked).length > 0) {
      this.btnTrace = true;
    } else {
      this.btnTrace = false;
    }
  }


  openManageTraceModal(data: any = null, mode: string) {
    const dialogRef = this.dialog.open(ModalTraceSettingComponent, {
      data: {
        mode: mode,
        data: data
      },
      width: "500px",
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
      this.btnTrace = false
    });
  }


  sortTable(n, tableId: string) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById(tableId);
    switching = true;
    dir = "asc";
    while (switching) {
      switching = false;
      rows = table.rows;
      for (i = 1; i < (rows.length - 1); i++) {
        shouldSwitch = false;
        x = rows[i].getElementsByTagName("TD")[n];
        y = rows[i + 1].getElementsByTagName("TD")[n];
        if (dir == "asc") {
          if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
            shouldSwitch = true;
            break;
          }
        } else if (dir == "desc") {
          if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
            shouldSwitch = true;
            break;
          }
        }
      }
      if (shouldSwitch) {
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
        switchcount++;
      } else {
        if (switchcount == 0 && dir == "asc") {
          dir = "desc";
          switching = true;
        }
      }
    }
  }


  onReturn(debt: any) {
    Swal.fire({
      title: "แจ้งเตือน !",
      text: "คุณต้องการยกเลิกการติดตามสัญญานี้หรือไม่ ?",
      icon: "question",
      confirmButtonText: "ตกลง",
      cancelButtonText: "ยกเลิก",
      showCancelButton: true,
      heightAuto: false
    }).then(btn => {
      if (btn.isConfirmed) {
        let data = new trdebtcollection();
        data = debt;
        data.traceBy = null;
        data.updatedBy = this.userInfo.user.username;
        data.updateDate = new Date();
        this.trDebtColectionService.createOrUpdate(debt).subscribe(result => {
          if (result.serviceResult.status === "Success") {
            Swal.fire({
              title: "Success !",
              text: "บันทึกสำเร็จ !",
              icon: "success",
              heightAuto: false
            });
            this.ngOnInit();
          }
        })
      }
    })
  }

  onDelete(debt: any) {
    Swal.fire({
      title: "แจ้งเตือน !",
      text: "คุณต้องการลบข้อมูลนี้หรือไม่ ?",
      icon: "question",
      confirmButtonText: "ตกลง",
      cancelButtonText: "ยกเลิก",
      showCancelButton: true,
      heightAuto: false
    }).then(btn => {
      if (btn.isConfirmed) {
        let data = new trdebtcollection();
        data = debt;
        data.traceBy = null;
        data.active = "N";
        data.updatedBy = this.userInfo.user.username;
        data.updateDate = new Date();
        this.trDebtColectionService.createOrUpdate(debt).subscribe(result => {
          if (result.serviceResult.status === "Success") {
            Swal.fire({
              title: "Success !",
              text: "บันทึกสำเร็จ !",
              icon: "success",
              heightAuto: false
            });
            this.ngOnInit();
          }
        })
      }
    })
  }

  searchFilter() {
    this.debtList = this.debtListMock.filter(x => x.companyContractNumber.includes(this.filter.searchTxt));
  }


  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.debtList?.data?.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  toggleAllRows() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }

    this.selection.select(...this.debtList.data);
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.debtList.filter = filterValue.trim().toLowerCase();
  }

}
