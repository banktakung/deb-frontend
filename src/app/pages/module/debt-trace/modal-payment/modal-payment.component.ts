import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Subject, takeUntil } from 'rxjs';
import { trpaid } from 'src/app/model/trpaid';
import { TrpaidService } from 'src/app/services/trpaid.service';
import Swal from 'sweetalert2';
import { ModalAddPaymentComponent } from './modal-add-payment/modal-add-payment.component';

@Component({
  selector: 'app-modal-payment-paid',
  templateUrl: './modal-payment.component.html',
  styleUrls: ['./modal-payment.component.scss']
})
export class ModalPaymentComponentPaid implements OnInit {
  private ngUnsubscribe = new Subject();
  public userInfo = JSON.parse(localStorage.getItem("userInfo"));
  public displayedColumns: string[] = ['round', 'payment', 'payDate', 'remark', 'other'];
  public dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public paidList: any;
  public page: string = "list";

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    private trPaidService: TrpaidService
  ) { }

  ngOnInit(): void {
    console.log(this.data);
    
    this.getPayList();
  }

  OnDestroy() {
    this.ngUnsubscribe.next(null);
    this.ngUnsubscribe.complete();
  }

  getPayList() {
    this.trPaidService.findAllByCompanyContact(this.data?.companyContractNumber).pipe(takeUntil(this.ngUnsubscribe)).subscribe(result => {
      if (result.serviceResult.status === "Success") {
        this.paidList = result.serviceResult.value;

        this.paidList = new MatTableDataSource(this.paidList);
        this.paidList.paginator = this.paginator;
        this.paidList.sort = this.sort;

      }
    }, err => {
      console.error(err);
    });
  }


  openAddPayment(data: any = null) {
    const dialogRef = this.dialog.open(ModalAddPaymentComponent, {
      data: {
        data: data,
        companyContractNumber: this.data?.companyContractNumber
      },
      width: "500px",
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }

  onDelete(obj: any) {
    Swal.fire({
      title: "คำเตือน !",
      text: "คุณต้องการลบข้อมูลนี้หรือไม่ ?",
      icon: "error",
      confirmButtonColor: "#2FC700",
      confirmButtonText: "ยืนยัน",
      cancelButtonText: "ยกเลิก",
      cancelButtonColor: "#FF0000",
      showCancelButton: true,
      showConfirmButton: true,
      heightAuto: false
    }).then(btn => {
      if (btn.isConfirmed) {
        let data = new trpaid();
        data = obj;
        data.active = "N";
        data.updateBy = this.userInfo.user.username;
        data.updateDate = new Date();
        this.trPaidService.createOrUpdate(data).subscribe(result => {
          if (result.serviceResult.status === "Success") {
            Swal.fire({
              title: "Success !",
              text: "บันทึกสำเร็จ",
              icon: "success",
              heightAuto: false
            });
            this.getPayList();
          } else {
            Swal.fire({
              title: "Error !",
              text: result.serviceResult.text,
              icon: "error",
              heightAuto: false
            })
          }
        }, err => {
          console.error(err);
          Swal.fire({
            title: "Error !",
            text: err.message,
            icon: "error",
            heightAuto: false
          });
        });
      }
    });
  }
}
