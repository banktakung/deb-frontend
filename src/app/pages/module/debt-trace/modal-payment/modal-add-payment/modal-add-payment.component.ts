import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { trpaid } from 'src/app/model/trpaid';
import { TrpaidService } from 'src/app/services/trpaid.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-modal-add-payment',
  templateUrl: './modal-add-payment.component.html',
  styleUrls: ['./modal-add-payment.component.scss']
})
export class ModalAddPaymentComponent implements OnInit {
  public trpaid = new trpaid();
  public userInfo = JSON.parse(localStorage.getItem("userInfo"));

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private trPaidService: TrpaidService,
    public dialogRef: MatDialogRef<ModalAddPaymentComponent>,
  ) { }

  ngOnInit(): void {
    if (this.data?.data) {
      this.trpaid = this.data.data
    }
  }

  onSave() {
    if (this.trpaid.payId) {
      this.trpaid.updateDate = new Date();
      this.trpaid.updateBy = this.userInfo.user.username;
    } else {
      this.trpaid.createBy = this.userInfo.user.username;
      this.trpaid.createDate = new Date();
      this.trpaid.updateDate = new Date();
      this.trpaid.updateBy = this.userInfo.user.username;
    }
    this.trpaid.active = "Y";
    this.trpaid.companyContractNumber = this.data?.companyContractNumber;

    this.trPaidService.createOrUpdate(this.trpaid).subscribe(result => {
      if (result.serviceResult.status === "Success") {
        Swal.fire({
          title: "Success !",
          text: "บันทึกสำเร็จ !",
          icon: "success",
          heightAuto: false
        });
        this.dialogRef.close();
      }
    }, err => {
      console.error(err);
    });
  }
}
