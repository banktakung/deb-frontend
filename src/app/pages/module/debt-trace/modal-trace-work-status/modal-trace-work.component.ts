import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-modal-trace-work',
  templateUrl: './modal-trace-work.component.html',
  styleUrls: ['./modal-trace-work.component.scss']
})
export class ModalTraceWorkStatusComponent implements OnInit {
  public data: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public modalData: any,
  ) { }

  ngOnInit(): void {
    this.data = this.modalData.data;
  }

}
