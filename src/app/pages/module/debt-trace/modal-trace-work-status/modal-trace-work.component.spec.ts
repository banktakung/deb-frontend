import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalTraceWorkStatusComponent } from './modal-trace-work.component';

describe('ModalTraceWorkStatusComponent', () => {
  let component: ModalTraceWorkStatusComponent;
  let fixture: ComponentFixture<ModalTraceWorkStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalTraceWorkStatusComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModalTraceWorkStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
