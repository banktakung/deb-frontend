import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import * as dayjs from 'dayjs';
import { Subject, takeUntil } from 'rxjs';
import { discountRate, minimumPayment } from 'src/app/core/middleclass';
import { trdebtcollection } from 'src/app/model/trdebtcollection';
import { MsdebtstatusService } from 'src/app/services/msdebtstatus.service';
import { TrdebtcollectionService } from 'src/app/services/trdebtcollection.service';
import Swal from 'sweetalert2';
import { ModalDebtManageComponent } from './modal-debt-manage/modal-debt-manage.component';
import { ModalTraceSettingComponent } from './modal-trace-setting/modal-trace-setting.component';
import { debounce } from 'lodash';
import { ModalImportExcelComponent } from './modal-import-excel/modal-import-excel.component';
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { ThemePalette } from '@angular/material/core';
import { ProgressSpinnerMode } from '@angular/material/progress-spinner';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
  selector: 'app-debt-contract',
  templateUrl: './debt-contract.component.html',
  styleUrls: ['./debt-contract.component.scss']
})
export class DebtContractComponent implements OnInit {
  private ngUnsubscribe = new Subject();
  public debtList: any;
  public debtListMock: any;
  public checked: boolean = false;
  public btnCreate: boolean = true;

  public displayedColumns: string[] = ['select', 'companyContractNumber', 'name', 'total', 'minimumPayment', 'discountRate', 'status', 'traceBy', 'phoneList', 'action'];
  public dataSource: MatTableDataSource<any>;

  public loadingColor: ThemePalette = 'primary';
  public loadingMode: ProgressSpinnerMode = 'determinate';
  public loadingValue = 10;

  public userInfo = JSON.parse(localStorage.getItem("userInfo"));
  public userRole = this.userInfo?.userRole.map(x => x.roleId).join(",");

  public debtStatusList: any;
  public filter = {
    searchTxt: '',
    debtStatus: '',
    startDate: dayjs(new Date().setDate(new Date().getDate() - 7)).format("YYYY-MM-DD"),
    endDate: dayjs(new Date().setDate(new Date().getDate() + 1)).format("YYYY-MM-DD")
  }

  selection = new SelectionModel<any>(true, []);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private trDebtColectionService: TrdebtcollectionService,
    public dialog: MatDialog,
    private msDebtStatusService: MsdebtstatusService
  ) {
    this.searchTxt = debounce(this.searchTxt, 1000);
  }

  ngOnInit(): void {
    this.getDebtList();
    this.getDebtStatus();
  }

  OnDestroy() {
    this.ngUnsubscribe.next(null);
    this.ngUnsubscribe.complete();
  }

  getDebtStatus() {
    this.msDebtStatusService.findAll().pipe(takeUntil(this.ngUnsubscribe)).subscribe(result => {
      if (result.serviceResult.status === "Success") {
        this.debtStatusList = result.serviceResult.value;
      }
    })
  }

  getDebtList(status: string = '') {
    this.trDebtColectionService.findByStatusTest(status, '', this.filter.searchTxt).pipe(takeUntil(this.ngUnsubscribe)).subscribe((event: HttpEvent<any>) => {
      if (event.type === HttpEventType.DownloadProgress) {
        this.loadingValue = +Math.round(event.loaded / event.total * 100);
      } else if (event.type === HttpEventType.Response) {
        const result = event.body;
        if (result.status === "Success") {
          this.debtList = result.value;
          for (let debt of this.debtList) {
            debt.minimumPayment = (+debt.fee + +debt.principle) + (0.2 * (+debt.breakInterest + +debt.interestNotRecognized));
            debt.discountRate = +debt.minimumPayment / +debt.totalAmount;
          }

          this.debtListMock = JSON.parse(JSON.stringify(this.debtList));

          this.debtList = new MatTableDataSource(this.debtList);
          this.debtList.paginator = this.paginator;
          this.debtList.sort = this.sort;
        } else {
          Swal.fire({
            title: "Error !",
            text: result.serviceResult.text,
            icon: "error",
            heightAuto: false
          })

        }
      }
    }, err => {
      console.error(err);
      Swal.fire({
        title: "Error !",
        text: err.message,
        icon: "error",
        heightAuto: false
      });
    });
  }

  searchTxt() {
    this.getDebtList(this.filter.debtStatus);
  }

  openManageModal(data: any = null) {
    const dialogRef = this.dialog.open(ModalDebtManageComponent, {
      data: data,
      width: "100%",
      height: "95%",
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getDebtList();
    });
  }

  openManageTraceModal(data: any = null, mode: string) {
    const dialogRef = this.dialog.open(ModalTraceSettingComponent, {
      data: {
        mode: mode,
        data: data
      },
      width: "500px",
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getDebtList();
      }
    });
  }

  checkAll() {
    let checked = !this.checked;

    if (checked) {
      for (let debt of this.debtList) {
        debt.checked = true;
      }
    } else {
      for (let debt of this.debtList) {
        debt.checked = false;
      }
    }

    this.check();
  }

  check(dataList: any = null) {
    let data = (dataList) ? dataList : this.debtList;
    let checkList = data.filter(x => x.checked === true);
    this.btnCreate = (checkList.length > 0) ? false : true;
  }

  sortTable(n) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("debtContact");
    switching = true;
    dir = "asc";
    while (switching) {
      switching = false;
      rows = table.rows;
      for (i = 1; i < (rows.length - 1); i++) {
        shouldSwitch = false;
        x = rows[i].getElementsByTagName("TD")[n];
        y = rows[i + 1].getElementsByTagName("TD")[n];
        if (dir == "asc") {
          if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
            shouldSwitch = true;
            break;
          }
        } else if (dir == "desc") {
          if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
            shouldSwitch = true;
            break;
          }
        }
      }
      if (shouldSwitch) {
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
        switchcount++;
      } else {
        if (switchcount == 0 && dir == "asc") {
          dir = "desc";
          switching = true;
        }
      }
    }
  }

  openImportExcel() {
    const dialogRef = this.dialog.open(ModalImportExcelComponent, {
      width: "500px",
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getDebtList();
    });
  }
  
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.debtList?.data?.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  toggleAllRows() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }

    this.selection.select(...this.debtList.data);
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.debtList.filter = filterValue.trim().toLowerCase();
  }
}
