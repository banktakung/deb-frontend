import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { trdebtcollection } from 'src/app/model/trdebtcollection';
import { MsdebtstatusService } from 'src/app/services/msdebtstatus.service';
import { SynotificationService } from 'src/app/services/synotification.service';
import { SyuserService } from 'src/app/services/syuser.service';
import { TrdebtcollectionService } from 'src/app/services/trdebtcollection.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-modal-trace-setting',
  templateUrl: './modal-trace-setting.component.html',
  styleUrls: ['./modal-trace-setting.component.scss']
})
export class ModalTraceSettingComponent implements OnInit {
  public syuserList: any;
  public trdebtCollection = new trdebtcollection();
  public userInfo = JSON.parse(localStorage.getItem("userInfo"));
  public debtStatus: any;
  public debtList: any;

  public setDebtStatus: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ModalTraceSettingComponent>,
    private syUserService: SyuserService,
    private trdebtCollectionService: TrdebtcollectionService,
    private msDebtStatusService: MsdebtstatusService,
    private syNotificationService: SynotificationService
  ) { }

  ngOnInit(): void {
    if (this.data?.mode === "assign") {
      this.onGetMember();
      this.trdebtCollection = this.data.data;
      this.onGetDebtStatus();
    }

    if (this.data?.mode === "work") {
      this.onGetDebtStatus();
      this.debtList = this.data.data;
    }
  }

  onGetMember() {
    this.syUserService.findAll().subscribe(result => {
      if (result.serviceResult.status === "Success") {
        this.syuserList = result.serviceResult.value;
      } else {
        Swal.fire({
          title: "Error !",
          text: result.serviceResult.text,
          icon: "error",
          heightAuto: false
        })
      }
    }, err => {
      console.error(err);
      Swal.fire({
        title: "Error !",
        text: err.message,
        icon: "error",
        heightAuto: false
      });
    });
  }

  onGetDebtStatus() {
    this.msDebtStatusService.findAll().subscribe(result => {
      if (result.serviceResult.status === "Success") {
        this.debtStatus = result.serviceResult.value;
      } else {
        Swal.fire({
          title: "Error !",
          text: result.serviceResult.text,
          icon: "error",
          heightAuto: false
        })
      }
    }, err => {
      console.error(err);
      Swal.fire({
        title: "Error !",
        text: err.message,
        icon: "error",
        heightAuto: false
      });
    });
  }

  onClose() {
    this.dialogRef.close(true);
  }

  onSave() {
    if (this.data?.mode === "assign") {
      if (this.data?.data?.length > 0) {
        let countComplete = 0;
        for (let data of this.data?.data) {
          let debtCollection = new trdebtcollection();
          debtCollection = data;
          debtCollection.traceBy = this.trdebtCollection.traceBy;
          debtCollection.status = this.setDebtStatus;
          debtCollection.updateDate = new Date();
          debtCollection.updatedBy = this.userInfo.user.username;
          this.trdebtCollectionService.createOrUpdate(debtCollection).subscribe(result => {
            if (result.serviceResult.status === "Success") {
              countComplete = countComplete + 1;
              if (countComplete === this.data?.data.length) {
                Swal.fire({
                  title: "Success !",
                  text: "บันทึกสำเร็จ !",
                  icon: "success",
                  heightAuto: false
                });
                this.onClose();
              }
            } else {
              Swal.fire({
                title: "Error !",
                text: result.serviceResult.text,
                icon: "error",
                heightAuto: false
              })
            }
          }, err => {
            console.error(err);
            Swal.fire({
              title: "Error !",
              text: err.message,
              icon: "error",
              heightAuto: false
            });
          });
        }
      } else {
        this.trdebtCollection.updatedBy = this.userInfo.user.username;
        this.trdebtCollection.status = this.setDebtStatus;
        this.trdebtCollection.updateDate = new Date();
        this.trdebtCollectionService.createOrUpdate(this.trdebtCollection).subscribe(result => {
          if (result.serviceResult.status === "Success") {
            Swal.fire({
              title: "Success !",
              text: "บันทึกสำเร็จ !",
              icon: "success",
              heightAuto: false
            });
            this.onClose();
            this.syNotificationService.sendSocket("debtTrace", null, this.setDebtStatus);
          } else {
            Swal.fire({
              title: "Error !",
              text: result.serviceResult.text,
              icon: "error",
              heightAuto: false
            })
          }
        }, err => {
          console.error(err);
          Swal.fire({
            title: "Error !",
            text: err.message,
            icon: "error",
            heightAuto: false
          });
        });
      }
    } else if (this.data?.mode === "work") {
      for (let debt of this.debtList) {
        let debtData = new trdebtcollection();
        debtData = debt;
        debtData.updatedBy = this.userInfo.user.username;
        debtData.updateDate = new Date();
        debtData.status = this.setDebtStatus;
        if (debtData['checked']) {
          this.trdebtCollectionService.createOrUpdate(debtData).subscribe(result => {
            if (result.serviceResult.status === "Success") {
              Swal.fire({
                title: "Success !",
                text: "บันทึกสำเร็จ !",
                icon: "success",
                heightAuto: false
              });
              this.syNotificationService.sendSocket("debtTrace", null, this.setDebtStatus);
              this.onClose();
            } else {
              Swal.fire({
                title: "Error !",
                text: result.serviceResult.text,
                icon: "error",
                heightAuto: false
              })
            }
          }, err => {
            console.error(err);
            Swal.fire({
              title: "Error !",
              text: err.message,
              icon: "error",
              heightAuto: false
            });
          });
        }
      }
    }
  }
}
