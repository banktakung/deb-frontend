import { TrdebtcollectionService } from './../../../services/trdebtcollection.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Subject, takeUntil } from 'rxjs';
import Swal from 'sweetalert2';
import * as dayjs from 'dayjs';
import { minimumPayment, discountRate } from 'src/app/core/middleclass';

@Component({
  selector: 'app-my-commission',
  templateUrl: './my-commission.component.html',
  styleUrls: ['./my-commission.component.scss']
})
export class MyCommissionComponent implements OnInit {
  private ngUnsubscribe = new Subject();
  public displayedColumns: string[] = ['companyContractNumber', 'totalAmount', 'statusName', 'shareAmount', 'commission'];
  public dataSource: MatTableDataSource<any>;
  public userInfo = JSON.parse(localStorage.getItem("userInfo"));
  public totalComission = 0;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private trDebtColectionService: TrdebtcollectionService
  ) { }

  ngOnInit(): void {
    this.getMyDebt();
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next(null);
    this.ngUnsubscribe.complete();
  }

  getMyDebt() {
    this.trDebtColectionService.findByStatus('', this.userInfo.user.username).pipe(takeUntil(this.ngUnsubscribe)).subscribe(result => {
      if (result.serviceResult.status === "Success") {
        let myDebtList = result.serviceResult.value;
        for (let debt of myDebtList) {
          debt.minimumPayment = minimumPayment(+debt.principle, +debt.breakInterest, +debt.fee);
          debt.paymentAll = minimumPayment(+debt.principle, +debt.breakInterest, +debt.fee, "totalPayment");
          debt.discountRate = discountRate(debt.paymentAll, debt.minimumPayment);
          debt.followDate = (debt.followDate) ? dayjs(debt.followDate).add(543, 'year').format("DD/MM/YYYY") : '';
          debt.shareAmount = debt.paymentAll * 0.30;
          debt.krittipak = debt.shareAmount * 0.30;
          debt.newCo = debt.shareAmount * 0.70;
          debt.callCenterCommission = debt.newCo * 0.15;
          this.totalComission = this.totalComission + debt.callCenterCommission;
        }
        
        this.dataSource = new MatTableDataSource(myDebtList);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      } else {
        Swal.fire({
          title: "Error !",
          text: result.serviceResult.text,
          icon: "error",
          heightAuto: false
        })
      }
    }, err => {
      console.error(err);
      Swal.fire({
        title: "Error !",
        text: err.message,
        icon: "error",
        heightAuto: false
      });
    });
  }

}
