import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Subject, takeUntil } from 'rxjs';
import { mscustomer } from 'src/app/model/mscustomer';
import { MscustomerService } from 'src/app/services/mscustomer.service';
import Swal from 'sweetalert2';
import { ModalPersonManageComponent } from './modal-person-manage/modal-person-manage.component';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss']
})
export class PersonComponent implements OnInit {
  public customerList: any;
  public userInfo = JSON.parse(localStorage.getItem("userInfo"));

  public displayedColumns: string[] = ['idcard', 'fname', 'phone', 'email', 'otherContact', 'addPerson'];
  public dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  private ngUnsubscribe = new Subject();

  constructor(
    public dialog: MatDialog,
    private msCustomerService: MscustomerService
  ) { }

  ngOnInit(): void {
    this.getCustomer();
  }

  OnDestroy() {
    this.ngUnsubscribe.next(null);
    this.ngUnsubscribe.complete();
  }

  getCustomer() {
    this.msCustomerService.findAll().pipe(takeUntil(this.ngUnsubscribe)).subscribe(result => {
      if (result.serviceResult.status === "Success") {
        this.customerList = result.serviceResult.value;
        this.dataSource = new MatTableDataSource(result.serviceResult.value);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      } else {
        Swal.fire({
          title: "Error !",
          text: result.serviceResult.text,
          icon: "error",
          heightAuto: false
        })
      }
    }, err => {
      console.error(err);
      Swal.fire({
        title: "Error !",
        text: err.message,
        icon: "error",
        heightAuto: false
      });
    });
  }

  openManageModal(data: any = null) {
    const dialogRef = this.dialog.open(ModalPersonManageComponent, {
      data: data,
      width: "100%",
      height: "95%",
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  deletePerson(obj: any) {
    Swal.fire({
      title: "แจ้งเตือน !",
      text: "คุณต้องการจะลบข้อมูลนี้หรือไม่ ?",
      icon: "question",
      showConfirmButton: true,
      confirmButtonColor: "#2FC700",
      confirmButtonText: "ตกลง",
      showCancelButton: true,
      cancelButtonText: "ยกเลิก",
      cancelButtonColor: "#FF0000"
    }).then(btn => {
      if (btn.isConfirmed) {
        let data: mscustomer = obj;
        data.active = "N";
        data.updatedBy = this.userInfo.user.username;
        data.updateDate = new Date();
        this.msCustomerService.createOrUpdate(data).subscribe(result => {
          if (result.serviceResult.status === "Success") {
            Swal.fire({
              title: "Success !",
              text: "บันทึกสำเร็จ !",
              icon: "success",
              heightAuto: false
            });
            this.getCustomer();
          } else {
            Swal.fire({
          title: "Error !",
          text: result.serviceResult.text,
          icon: "error",
          heightAuto: false
        })
          }
        }, err => {
          console.error(err);
          Swal.fire({
        title: "Error !",
        text: err.message,
        icon: "error",
        heightAuto: false
      });
        })
      }
    });
  }
}
