import { Component, OnInit } from '@angular/core';
import { syUserForm } from 'src/app/model/syuser';
import { SyuserService } from 'src/app/services/syuser.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-member-setting',
  templateUrl: './member-setting.component.html',
  styleUrls: ['./member-setting.component.scss']
})
export class MemberSettingComponent implements OnInit {
  public syUser = new syUserForm();
  public userInfo = JSON.parse(localStorage.getItem("userInfo"));

  constructor(
    private syUserService: SyuserService
  ) { }

  ngOnInit(): void {
    this.syUser = this.userInfo.user;
    delete this.syUser.password;
  }

  onSubmit() {
    if (!this.syUser.name) {
      Swal.fire({
        title: "Warning !",
        text: "กรุณากรอกชื่อ !",
        icon: "warning",
        heightAuto: false
      });
      return;
    }

    if (this.syUser.password && (this.syUser.password != this.syUser.rePassword)) {
      Swal.fire({
        title: "Warning !",
        text: "รหัสผ่านไม่ตรงกัน !",
        icon: "warning",
        heightAuto: false
      });
      return;
    }

    this.syUserService.createOrUpdate(this.syUser).subscribe(result => {
      if (result.serviceResult.status === "Success") {
        Swal.fire({
              title: "Success !",
              text: "บันทึกสำเร็จ !",
              icon: "success",
              heightAuto: false
            });
      }
    })
  }
}
