import { Component, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import LinearGradient from 'zrender/lib/graphic/LinearGradient';
import { TrdebtcollectionService } from 'src/app/services/trdebtcollection.service';
import * as dayjs from 'dayjs';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  private ngUnsubscribe = new Subject();
  options: any;
  public dataYear: any;
  public dataMonth: any;
  public dataWeek: any;
  public summaryYear: any;
  public summaryMonth: any;
  public summaryWeek: any;
  public monthName = (new Date()).toLocaleString('th-TH', { month: 'long' });
  public filter = {
    year: dayjs(new Date()).add(543, 'year').format("YYYY"),
    month: dayjs(new Date()).format("MM"),
    day: dayjs(new Date()).format("DD")
  }
  public maxYearPrevius: number = 20;
  public yearList: any;
  constructor(
    private trDebtcollection: TrdebtcollectionService

  ) {
    this.genYearList();
   }

   view: any[] = [700, 400];

   // options
   showXAxis = true;
   showYAxis = true;
   gradient = false;
   showLegend = true;
   showXAxisLabel = true;
   xAxisLabel = 'Country';
   showYAxisLabel = true;
   yAxisLabel = 'Population';

   colorScheme = {
     domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
   };

  ngOnInit(): void {
    let date = new Date(2022, 4, 0).getDate();
    console.log(date);

    this.getSummary();

  }

   getSummary() {
    let year = +this.filter.year - 543;
    let month = +this.filter.month;
    let day = +this.filter.day;
    this.trDebtcollection.summary(year.toString(),month.toString(),day.toString()).pipe(takeUntil(this.ngUnsubscribe)).subscribe(result => {
      if (result.serviceResult.status === "Success") {
        console.log(result.serviceResult.value);

          this.dataYear = result.serviceResult.value.year;
          this.dataMonth = result.serviceResult.value.month;
          this.dataWeek = result.serviceResult.value.week;
          this.summaryYear = result.serviceResult.value.summaryYear;
          this.summaryMonth = result.serviceResult.value.summaryMonth;
          this.summaryWeek = result.serviceResult.value.summaryWeek;
        // this.dataCustomerSummary = result.serviceResult.value.customerYear;
        // this.dataStatusCustomer.data = result.serviceResult.value.caseCustomer[0].value;
        // this.dataStatusCustomer.mock = result.serviceResult.value.caseCustomer;
      } else {
        // this.toast.error("ไม่สามารถเชื่อมต่อฐานข้อมูลได้");
      }
    }, err => {
      console.error(err);
      // this.toast.error(err.message);
    })
  }

  genYearList() {
    let list = [];
    for (let i = 0; i < this.maxYearPrevius; i++) {
      let year = dayjs(dayjs(new Date()).add(543, 'year').format("YYYY-MM-DD")).add(-i, 'year').format("YYYY");
      list.push(year);
    }
    this.yearList = list;
  }


}
