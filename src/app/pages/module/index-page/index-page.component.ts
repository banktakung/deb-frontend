import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DeviceDetectorService } from 'ngx-device-detector';
import { Location } from '@angular/common';
import { Subject, takeUntil } from 'rxjs';
import { TrdebtcollectionService } from 'src/app/services/trdebtcollection.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-index-page',
  templateUrl: './index-page.component.html',
  styleUrls: ['./index-page.component.scss']
})
export class IndexPageComponent implements OnInit {
  private ngUnsubscribe = new Subject();
  public page: string = "dashboard";
  public device: any;
  public userInfo = JSON.parse(localStorage.getItem("userInfo"));
  public userRole = this.userInfo?.userRole.map(x => x.roleId).join(",");
  public sideMenu: any;
  public title: string = "Dashboard";

  constructor(
    public router: Router,
    private location: Location,
    private deviceService: DeviceDetectorService,
    private routerActive: ActivatedRoute,
  ) {
    if (this.routerActive.params['value']['page']) this.page = this.routerActive.params['value']['page'];
    this.device = this.deviceService.deviceType;
    if (!this.userInfo) {
      this.router.navigate(['login']);
    }
  }


  ngOnInit(): void {
    if (this.userInfo) {
      this.userMenu();

      if (this.page) {
        if (this.page === 'member-setting') {
          this.title = "ตั้งค่าบัญชี";
        } else {
          this.title = this.sideMenu.filter(x => x.pathLink.includes(this.page))[0].name;
        }
      }
    }
  }

  OnDestroy() {
    this.ngUnsubscribe.next(null);
    this.ngUnsubscribe.complete();
  }

  userMenu() {
    if (this.userRole.includes("1")) {
      this.sideMenu = [
        {
          name: 'แดชบอร์ด',
          icon: 'equalizer',
          pathLink: 'dashboard'
        }, {
          name: 'รายการสัญญา',
          icon: 'description',
          pathLink: 'debt'
        }, {
          name: 'รายการสัญญา D2U',
          icon: 'content_paste_search',
          pathLink: 'debtTrace'
        }, {
          name: 'รายการสัญญาอายัด',
          icon: 'content_paste_search',
          pathLink: 'freezeTrace'
        }, {
          name: 'รายการสัญญายึดทรัพย์',
          icon: 'content_paste_search',
          pathLink: 'sequeststerTrace'
        }, {
          name: 'รายชื่อบุคคล',
          icon: 'groups',
          pathLink: 'person'
        }, {
          name: 'คอมมิชชั่นของฉัน',
          icon: 'payments',
          pathLink: 'mycommission'
        }, {
          name: 'จัดการ User',
          icon: 'manage_accounts',
          pathLink: 'user'
        }, {
          name: 'ตั้งค่าคอมมิชชัน',
          icon: 'attach_money',
          pathLink: 'commission'
        }, {
          name: 'ตั้งค่าเป้าหมาย',
          icon: 'flag',
          pathLink: 'target'
        }, {
          name: 'Export Data',
          icon: 'storage',
          pathLink: 'export'
        },
      ]
    } else if (this.userRole.includes("2")) {
      this.sideMenu = [
        {
          name: 'แดชบอร์ด',
          icon: 'equalizer',
          pathLink: './'
        }, {
          name: 'รายการสัญญา D2U',
          icon: 'content_paste_search',
          pathLink: 'debtTrace'
        }, {
          name: 'รายชื่อบุคคล',
          icon: 'groups',
          pathLink: 'person'
        }, {
          name: 'คอมมิชชั่นของฉัน',
          icon: 'payments',
          pathLink: 'mycommission'
        }
      ]
    } else if (this.userRole.includes("3")) {
      this.sideMenu = [
        {
          name: 'แดชบอร์ด',
          icon: 'equalizer',
          pathLink: './'
        }, {
          name: 'รายการสัญญา',
          icon: 'description',
          pathLink: 'debt'
        }, {
          name: 'งานเอกสาร',
          icon: 'content_paste_search',
          pathLink: 'debtTrace'
        }, {
          name: 'รายชื่อบุคคล',
          icon: 'groups',
          pathLink: 'person'
        }, {
          name: 'คอมมิชชั่นของฉัน',
          icon: 'payments',
          pathLink: 'mycommission'
        }
      ]
    } else if (this.userRole.includes("4")) {
      this.sideMenu = [
        {
          name: 'แดชบอร์ด',
          icon: 'equalizer',
          pathLink: './'
        }, {
          name: 'รายการสัญญาอายัด',
          icon: 'content_paste_search',
          pathLink: 'freezeTrace'
        }, {
          name: 'รายชื่อบุคคล',
          icon: 'groups',
          pathLink: 'person'
        }, {
          name: 'คอมมิชชั่นของฉัน',
          icon: 'payments',
          pathLink: 'mycommission'
        }
      ]
    } else if (this.userRole.includes("5")) {
      this.sideMenu = [
        {
          name: 'แดชบอร์ด',
          icon: 'equalizer',
          pathLink: './'
        }, {
          name: 'รายการสัญญายึดทรัพย์',
          icon: 'content_paste_search',
          pathLink: 'sequeststerTrace'
        }, {
          name: 'รายชื่อบุคคล',
          icon: 'groups',
          pathLink: 'person'
        }, {
          name: 'คอมมิชชั่นของฉัน',
          icon: 'payments',
          pathLink: 'mycommission'
        }
      ]
    }

  }


  logout() {
    localStorage.clear();
    this.router.navigate(["./login"]);
  }

  changeMenu(module: string, moduleName: string) {
    this.page = module;
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    // this.location.replaceState('/' + module);
    this.router.navigate(['./' + module]);
  }

}
