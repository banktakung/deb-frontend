import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject, takeUntil } from 'rxjs';
import { SycommissiontypeService } from 'src/app/services/sycommissiontype.service';
import Swal from 'sweetalert2';
import { debounce } from 'lodash';

@Component({
  selector: 'app-commission',
  templateUrl: './commission.component.html',
  styleUrls: ['./commission.component.scss']
})
export class CommissionComponent implements OnInit {
  private ngUnsubscribe = new Subject();
  public userInfo = JSON.parse(localStorage.getItem("userInfo"));
  public typeList: any;
  public editorData = {
    typeId: null,
    name: null,
    value: null
  }

  constructor(
    private syCommissionTypeService: SycommissiontypeService,
    private snackBar: MatSnackBar
  ) {
  }

  ngOnInit(): void {
    this.getCommission();
  }

  OnDestroy() {
    this.ngUnsubscribe.next(null);
    this.ngUnsubscribe.complete();
  }

  getCommission() {
    this.syCommissionTypeService.findAll().pipe(takeUntil(this.ngUnsubscribe)).subscribe(result => {
      if (result.serviceResult.status === "Success") {
        this.typeList = result.serviceResult.value;
      }
    }, err => {
      console.error(err);
    })
  }

  onSave(data: any) {
    data.updateDate = new Date();
    data.updateBy = this.userInfo.user.username;
    this.syCommissionTypeService.createOrUpdate(data).subscribe(result => {
      if (result.serviceResult.status !== "Success") {
        Swal.fire({
          title: "Error !",
          text: result.serviceResult.text,
          icon: "error",
          heightAuto: false
        })
      }
    }, err => {
      console.error(err);
    })
  }
}
