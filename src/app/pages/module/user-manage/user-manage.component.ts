import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Subject, takeUntil } from 'rxjs';
import { syuser } from 'src/app/model/syuser';
import { SyuserService } from 'src/app/services/syuser.service';
import Swal from 'sweetalert2';
import { ModalManageUserComponent } from './modal-manage-user/modal-manage-user.component';

@Component({
  selector: 'app-user-manage',
  templateUrl: './user-manage.component.html',
  styleUrls: ['./user-manage.component.scss']
})
export class UserManageComponent implements OnInit {
  private ngUnsubscribe = new Subject();
  public userInfo = JSON.parse(localStorage.getItem("userInfo"));
  public displayedColumns: string[] = ['username', 'name', 'action'];

  public userList: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private syUserService: SyuserService,
    public snackBar: MatSnackBar,
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.getUser();
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next(null);
    this.ngUnsubscribe.complete();
  }

  getUser() {
    this.syUserService.findAll().pipe(takeUntil(this.ngUnsubscribe)).subscribe(result => {
      if (result.serviceResult.status === "Success") {
        this.userList = result.serviceResult.value;
        this.userList = new MatTableDataSource(this.userList);
        this.userList.paginator = this.paginator;
        this.userList.sort = this.sort;
      } else {
        this.snackBar.open("ไม่สามารถดำเนินการต่อได้ !", "รับทราบ", { duration: 5000, verticalPosition: 'bottom', horizontalPosition: 'right' });
      }
    }, err => {
      this.snackBar.open(err.message, "รับทราบ", { duration: 5000, verticalPosition: 'bottom', horizontalPosition: 'right' });
    });
  }

  openManageUser(data: any = null) {
    const dialogRef = this.dialog.open(ModalManageUserComponent, {
      data: data,
      width: "500px",
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getUser();
    });
  }

  onDelete(row: any) {
    Swal.fire({
      title: "คำเตือน !",
      text: "คุณต้องการลบข้อมูลนี้หรือไม่ ?",
      icon: "error",
      confirmButtonColor: "#2FC700",
      confirmButtonText: "ยืนยัน",
      cancelButtonText: "ยกเลิก",
      cancelButtonColor: "#FF0000",
      showCancelButton: true,
      showConfirmButton: true,
      heightAuto: false
    }).then(btn => {
      if (btn.isConfirmed) {
        let data = new syuser();
        data.active = "N";
        data.username = row.username;
        data.updateDate = new Date();
        data.updatedBy = this.userInfo.user.username;
        this.syUserService.createOrUpdate(data).subscribe(result => {
          if (result.serviceResult.status === "Success") {
            Swal.fire({
              title: "Success !",
              text: "บันทึกสำเร็จ !",
              icon: "success",
              heightAuto: false
            });
            this.getUser();
          } else {
            Swal.fire("Error !", result.serviceResult.text, "error");
          }
        }, err => {
          console.error(err);
        })
      }
    })
  }
}
