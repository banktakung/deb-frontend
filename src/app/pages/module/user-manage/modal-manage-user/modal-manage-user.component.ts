import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject, takeUntil } from 'rxjs';
import { syUserForm } from 'src/app/model/syuser';
import { SyroleService } from 'src/app/services/syrole.service';
import { SyuserService } from 'src/app/services/syuser.service';
import { SyuserroleService } from 'src/app/services/syuserrole.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-modal-manage-user',
  templateUrl: './modal-manage-user.component.html',
  styleUrls: ['./modal-manage-user.component.scss']
})
export class ModalManageUserComponent implements OnInit {
  private ngUnsubscribe = new Subject();
  public syUser = new syUserForm();
  public roleList: any;
  public userInfo: any = JSON.parse(localStorage.getItem("userInfo"));

  constructor(
    private syUserService: SyuserService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    private syRoleService: SyroleService,
    private syUserRoleService: SyuserroleService,
    public dialogRef: MatDialogRef<ModalManageUserComponent>
  ) { }

  ngOnInit(): void {
    this.getRole();
    if (this.data) {
      this.syUser = this.data;
      delete this.syUser.password;
    }
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next(null);
    this.ngUnsubscribe.complete();
  }

  getRole() {
    this.syRoleService.findAll().pipe(takeUntil(this.ngUnsubscribe)).subscribe(result => {
      if (result.serviceResult.status === "Success") {
        this.roleList = result.serviceResult.value;

        if (this.data) {
          this.getUserRole();
        }
      }
    })
  }

  getUserRole() {
    this.syUserRoleService.findByUserName(this.data.username).pipe(takeUntil(this.ngUnsubscribe)).subscribe(result => {
      if (result.serviceResult.status === "Success") {
        let objResult = result.serviceResult.value;
        for (let role of this.roleList) {
          for (let userRole of objResult) {
            if (role.roleId === userRole.roleId) {
              role.checked = true;
            }
          }
        }
      }
    })
  }

  onSubmit() {
    this.syUser.updateDate = new Date();
    this.syUser.updatedBy = this.userInfo.user.username;
    if (!this.syUser.createdBy) {
      this.syUser.createdBy = this.userInfo.user.username;
      this.syUser.createDate = new Date();
    }
    this.syUser.active = "Y";

    this.syUserService.createOrUpdate(this.syUser).subscribe(result => {
      if (result.serviceResult.status === "Success") {
        // Insert Role
        this.roleList = this.roleList.filter(x => x.checked)
        for (let role of this.roleList) {
          role.username = this.data.username;
          delete role.checked;
        }
        this.syUserRoleService.bulkCreate(this.roleList).subscribe(result => {
          if (result.serviceResult.status === "Success") {
            Swal.fire({
              title: "Success !",
              text: "บันทึกสำเร็จ !",
              icon: "success",
              heightAuto: false
            });
            this.dialogRef.close(true);
          } else {
            Swal.fire("Error !", result.serviceResult.text, "error");
          }
        }, err => {
          console.error(err);
          Swal.fire("Error !", err.message, "error");
        });
      }
    })
  }
}
