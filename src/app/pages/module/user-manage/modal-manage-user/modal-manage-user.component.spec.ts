import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalManageUserComponent } from './modal-manage-user.component';

describe('ModalManageUserComponent', () => {
  let component: ModalManageUserComponent;
  let fixture: ComponentFixture<ModalManageUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalManageUserComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModalManageUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
