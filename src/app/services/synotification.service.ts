import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigServerService } from '../core/config-server.service';
import { synotification } from '../model/synotificationmodel';

@Injectable({
    providedIn: 'root'
})
export class SynotificationService {
    public userInfo: any = JSON.parse(localStorage.getItem("userInfo"));

    constructor(private httpClient: HttpClient, private configService: ConfigServerService) { }

    public createOrUpdate(data: any) {
        return this.httpClient.post<any>(this.configService.getAPI('synotification/save'), data).pipe(
            map(respons => {
                return {
                    serviceResult: respons
                }
            }));
    }

    public sendSocket(formCode: string, refId: string = '', data: string = '', memberId: string = '') {
        let notification = new synotification();
        notification.module = "Socket";
        notification.event = "reloadData";
        notification.status = "WAIT";
        notification.active = "Y";
        notification.createBy = this.userInfo.user.username;
        notification.createDate = new Date();
        notification.updateBy = this.userInfo.user.username;
        notification.updateDate = new Date();
        if (refId) notification.refId = refId;
        if (formCode) notification.formCode = formCode;
        if (data) notification.data = data;
        if (memberId) notification.member_id = +memberId;

        this.createOrUpdate(notification);
    }

    public findAll() {
        return this.httpClient.get<any>(this.configService.getAPI('synotification/findAll')).pipe(
            map(respons => {
                return {
                    serviceResult: respons
                }
            }));
    }

    public findByPk(value: string) {
        return this.httpClient.get<any>(this.configService.getAPI('synotification/findByPk?value=' + value)).pipe(
            map(respons => {
                return {
                    serviceResult: respons
                }
            }));
    }


}