import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { ConfigServerService } from '../core/config-server.service';

@Injectable({
    providedIn: 'root'
})
export class TrdebtcollectionService {

    constructor(private httpClient: HttpClient, private configService: ConfigServerService) { }

    public createOrUpdate(data: any) {
        return this.httpClient.post<any>(this.configService.getAPI('trdebtcollection/save'), data).pipe(
            map(respons => {
                return {
                    serviceResult: respons
                }
            }));
    }

    public importFromExcel(data: any) {
        return this.httpClient.post<any>(this.configService.getAPI('trdebtcollection/importFromExcel'), data).pipe(
            map(respons => {
                return {
                    serviceResult: respons
                }
            }));
    }
    

    public findAll() {
        return this.httpClient.get<any>(this.configService.getAPI('trdebtcollection/findAll')).pipe(
            map(respons => {
                return {
                    serviceResult: respons
                }
            }));
    }

    public findByPk(value: string) {
        return this.httpClient.get<any>(this.configService.getAPI('trdebtcollection/findByPk?value=' + value)).pipe(
            map(respons => {
                return {
                    serviceResult: respons
                }
            }));
    }

    public findByStatusTest(statusCode: string = '', username: string = '', searchTxt: string = '') {
        let param = "?statusCode=" + statusCode + "&username=" + username + "&searchTxt=" + searchTxt;
        const req = new HttpRequest("GET", this.configService.getAPI('trdebtcollection/findByStatus' + param), {
            reportProgress: true  // this is important!
        });

        return this.httpClient.request(req);
        // return this.httpClient.get<any>(this.configService.getAPI('trdebtcollection/findByStatus' + param), { reportProgress: true }).pipe(
        //     map(respons => {
        //         return {
        //             serviceResult: respons
        //         }
        //     }));
    }

    public findByStatus(statusCode: string = '', username: string = '', searchTxt: string = '') {
        let param = "?statusCode=" + statusCode + "&username=" + username + "&searchTxt=" + searchTxt;
        return this.httpClient.get<any>(this.configService.getAPI('trdebtcollection/findByStatus' + param)).pipe(
            map(respons => {
                return {
                    serviceResult: respons
                }
            }));
    }

    public findAllTrace() {
        return this.httpClient.get<any>(this.configService.getAPI('trdebtcollection/findAllTrace')).pipe(
            map(respons => {
                return {
                    serviceResult: respons
                }
            }));
    }

    public findMyTrace(status: string) {
        return this.httpClient.get<any>(this.configService.getAPI('trdebtcollection/findMyTrace?status=' + status)).pipe(
            map(respons => {
                return {
                    serviceResult: respons
                }
            }));
    }


    public summary(year: string = '', month: string = '', day: string = '') {
        let param ="?year=" + year + "&month=" + month + "&day=" + day;
        return this.httpClient.get<any>(this.configService.getAPI('trdebtcollection/dashboardSummary'+ param)).pipe(
            map(respons => {
                return {
                    serviceResult: respons
                }
            }));
    }



}