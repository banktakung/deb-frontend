export class trpaid {
    payId: number;
    companyContractNumber: string;
    payment: number;
    payDate: Date;
    remark: string;
    active: string;
    createBy: string;
    createDate: Date;
    updateBy: string;
    updateDate: Date;
};