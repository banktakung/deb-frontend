export class synotification {
    notificationId: number;
    refId: string;
    module: string;
    formCode: string;
    event: string;
    data: string;
    member_id: number;
    status: string;
    active: string;
    createBy: string;
    createDate: Date;
    updateBy: string;
    updateDate: Date;
};