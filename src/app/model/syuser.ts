export class syuser {
    username: string;
    user_login: string;
    name: string;
    password: string;
    Token: string;
    entryposition: string;
    picture: string;
    groupname: string;
    active: string;
    remark: string;
    createdBy: string;
    createDate: Date;
    updatedBy: string;
    updateDate: Date;
    sourceId: string;
    source: string;
};

export class syUserForm extends syuser {
    rePassword: string;
}